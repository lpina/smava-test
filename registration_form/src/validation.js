import isEmail from 'validator/lib/isEmail'
import isAlpha from 'validator/lib/isAlpha'
import {isValid as isValidIBAN} from 'iban'

const isNotEmpty = (value = '') => value.length > 0

// TODO:
// it could be helpful to allow spaces between words as well
// If that were to be supported, some extra rules might make sense:
//  -> edge case: name should not contain only whitespace
//  -> edge case: don't allow sequence of multiple whitespaces (or trim to one)
//  -> don't forget to .trim() value to avoid surrounding whitespace
export const alpha = (value, label) => 
  isAlpha(value) ? false : `${label} should contain only letters`

export const notEmpty = (value, label) => 
  isNotEmpty(value) ? false : `${label} is required`

export const email = (value, label) => 
  isEmail(value) ? false : 'Value should be a valid email'

export const validIBAN = (value, label) => 
  isValidIBAN(value) ? false : `Value should be a valid IBAN`
