import React from 'react'

import './App.css'
import RegistrationForm from '../RegistrationForm'

const App = () =>
  <div className="app" >
    <RegistrationForm />
  </div>

export default App
