import React, { Component } from 'react'

import './RegistrationForm.css'
import {fieldsConfig} from '../config'

import ValidatedInput from '../ValidatedInput'
import BankAccount from '../BankAccount'

  // TODO !!
  /* 
    There is some duplication going on here (in state setters and click handlers
    ) due to having an extra level (account index) in 
    state.accounts[{}] vs state.values{}.

    A possible improvement would be to have multiple forms, one for
    the user registration data, and one for each bank account.
    Each of those forms would have the simpler state structure { values:{} }.

    This would require defining a generic Form component with fieldIDs passed
    as props (instead of having the fields hardcoded in the render function).
  */


// -----------------------------------------------------------------------------
// Util
// -----------------------------------------------------------------------------

const initialFieldValue = { value: '', error: false }

/**
 *  Generates a state updater function for state.bankAccounts.
 *  newstate may contain 'error' and/or 'value' keys
 */
const setBankAccount = ({accountIndex, fieldID, ...newState}) => (prevState) => {
  // clone bankAccounts property and update field value
  let {bankAccounts} = prevState
  let currentState = bankAccounts[accountIndex][fieldID]
  bankAccounts[accountIndex][fieldID] = {...currentState, ...newState}
  return {bankAccounts}
}

/**
 * Generates a state updater function for state.values 
 * newstate may contain 'error' and/or 'value' keys
 * */

const setField = ({fieldID, ...newState}) => (prevState) => {
  const prevValues = prevState.values
  const prevFieldState = prevValues[fieldID]
  return {
    values: {
      ...prevValues,
      [fieldID]: {...prevFieldState, ...newState}
    }
  }
}

/**
 * Validates a field by invoking each validator function from fieldsConfig
 * until an error is found.
 * If an error exists, the error field is set to a string with an error message
 * If there are no errors, the error field is set to false
 */
const validateField = (fieldID, value) => {
  let errorMessage = false
  const {validators, label} = fieldsConfig[fieldID]

  // Iterate validator functions
  // Set error message and break if an error is found
  validators.every((validator) => {
    const error = validator(value, label)
    if(error !== false) errorMessage = error
    // Iteration will stop if an error is set
    return error !== false
  })

  return errorMessage
}

// -----------------------------------------------------------------------------
// Component
// -----------------------------------------------------------------------------

class RegistrationForm extends Component {

  // SET INITIAL STATE
  constructor(props){
    super(props)
    this.state={
      values: {
        firstName: initialFieldValue,
        lastName: initialFieldValue,
        email: initialFieldValue,
      },
      bankAccounts: [],
      showEmptyAccountsError: false
    }
  }

  // ---------------------------------------------------------------------------
  // EVENT HANDLERS
  // ---------------------------------------------------------------------------

  onInputChange = (fieldID) => (event) => {
    const value = event.target.value
    this.setState( setField({ fieldID, value }) )
  }

  onAccountInputChange = (accountIndex) => (fieldID) => (event) => {
    const value = event.target.value
    this.setState( setBankAccount({accountIndex, fieldID, value}) )
  }

  onInputBlur = (fieldID) => (event) => {
    const value = event.target.value
    const error = validateField(fieldID, value)
    this.setState( setField({ fieldID, value, error }) )
  }

  onAccountInputBlur = (accountIndex) => (fieldID) => (event) => {
    const value = event.target.value
    const error = validateField(fieldID, value)
    this.setState( setBankAccount({accountIndex, fieldID, value, error}) )
  }

  onAddAccount = () => {
    this.setState( ({bankAccounts}) => ({
      bankAccounts : [
        ...bankAccounts,
        { iban: initialFieldValue, bankName: initialFieldValue }
      ]
    }) )
  }

  onDeleteAccount = (index) => () => {
    this.setState( ({bankAccounts}) => ({
      bankAccounts: [
        ...bankAccounts.slice(0, index),
        ...bankAccounts.slice(index + 1),
      ]
    }) )
  }

  /**
   * Runs validation for all fields
   * if all are valid, shows pop up with form data
   * if there are invalid fields, update state to idsplay errors
   */
  onSubmitForm = (event) => {
    event.preventDefault()

    let errorsInForm = false
    const state = this.state

    const validate = (values) => Object.keys(values).reduce( (output, fieldID) => {
      let {value, error} = values[fieldID]
      // errors may not exist if the field hasn't been touched yet,
      // so it's safer to check again on submit.
      // An alternative could be using a flag to check if fields are pristine
      error = error || validateField(fieldID, value)
      if(error !== false) errorsInForm = true
      return { ...output, [fieldID]: {value, error}}
    }, {})

    const validatedValues = validate(state.values)
    const validatedBankAccounts = state.bankAccounts.map(validate)

    // Also check for empty bank accounts
    if(errorsInForm === false && validatedBankAccounts.length > 0){
      
      // TODO !!
      /*
        properly format the form data so it only displays field
        values instead of the { value, error } object
      */
      alert(
        `Form data: \n ${JSON.stringify({ 
            ...state.values, bankAccounts: state.bankAccounts },
          null,2)}`
      )
    } else {
      this.setState( () => ({
        values: validatedValues,
        bankAccounts: validatedBankAccounts,
        showEmptyAccountsError: validatedBankAccounts.length === 0,
      }))
    }
  }

  // ---------------------------------------------------------------------------
  // RENDERING
  // ---------------------------------------------------------------------------

  render() {
    const {values, bankAccounts, showEmptyAccountsError} = this.state
    return (
      <form className="registration-form" onSubmit={this.onSubmitForm} >
        <div className="registration-form__heading" >Register account</div>
        {['firstName', 'lastName', 'email'].map( (fieldID, index) =>
          <ValidatedInput
            key={`${fieldID}-${index}`}
            label={fieldsConfig[fieldID].label}
            value={values[fieldID].value}
            error={values[fieldID].error}
            onChange={this.onInputChange(fieldID)}
            onBlur={this.onInputBlur(fieldID)}
          />
        )}

        <div className="registration-form__heading" >Bank Accounts</div>

        { (showEmptyAccountsError && bankAccounts.length === 0) &&
          <div className="registration-form__error" >
            You should provide at least one bank account
          </div>
        }
        { bankAccounts.map( (account, index) =>
            <BankAccount
              key={`${account.iban}-${index}`}
              onDeleteAccount={this.onDeleteAccount(index)}
              onInputChange={this.onAccountInputChange(index)}
              onInputBlur={this.onAccountInputBlur(index)}
              fields={account}
            />
        )}

        <button type="button" onClick={this.onAddAccount} >
          + Add bank account
        </button>
        <input type="submit" value="Submit!" />
      </form>
    );
  }
}

export default RegistrationForm;
