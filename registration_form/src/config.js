import {alpha, notEmpty, email, validIBAN} from './validation'

export const fieldsConfig = {
  'firstName': {
    label:'First name',
    validators: [alpha, notEmpty]
  },
  'lastName': {
    label:'Last name',
    validators: [alpha, notEmpty]
  },
  'email': {
    label:'Email',
    validators: [email, notEmpty]
  },
  'iban': {
    label:'IBAN',
    validators: [validIBAN, notEmpty]
  },
  'bankName': {
    label: 'Bank name',
    validators: [notEmpty]
  },
}
