import React from 'react'

import {fieldsConfig} from '../config'
import ValidatedInput from '../ValidatedInput'
import trashIcon from '../assets/trash.svg'

const BankAccount = ({onDeleteAccount, onInputChange, onInputBlur, fields}) =>
  <div className="bank-account" style={{position: 'relative'}}>

    <div onClick={onDeleteAccount}>
      <img src={trashIcon} style={{
        position: 'absolute',
        height: '20px',
        right: '16px',
        top: '26px',
      }} alt="logo"
      />
    </div>

    {['iban', 'bankName'].map( (fieldID, index) =>
      <ValidatedInput
        key={`${fieldID}-${index}`}
        label={fieldsConfig[fieldID].label}
        value={fields[fieldID].value}
        error={fields[fieldID].error}
        onChange={onInputChange(fieldID)}
        onBlur={onInputBlur(fieldID)}
      />
    )}

  </div>

export default BankAccount
