import React from 'react'

import './ValidatedInput.css'

const ValidatedInput = ({label, error, value, onChange, onBlur}) =>
  <div className="validated-input" >
    <div className="validated-input__label" >{label}</div>
    <input
      className="validated-input__input"
      type="text"
      value={value}
      onChange={onChange}
      onBlur={onBlur}
    />
    { error !== false &&
      <div className="validated-input__error" >{error}</div>
    }
  </div>

export default ValidatedInput
