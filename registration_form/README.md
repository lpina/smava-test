For the registration form task, I decided to use React.

Since time is a factor in this test, I took some steps to make things go faster:

- In order to save time setting up the project, I'm using [create-react-app](https://github.com/facebookincubator/create-react-app)
, a boilerplate generator for react. 
 It creates a project with bundling/testing/linting/scripts config and basic folder 
 structure.
- I skipped implementing tests
- I used the [validator](https://github.com/chriso/validator.js/) lib for input 
validation, mostly because I didn't want to implement the validatoin for the 
email field

I did do one thing that slowed me down a bit: not using lodash for object/array
manipulation and functional utils.  
I thought it was worth the cost so you could better evaluate my skills with 
javaScript.


I didn't stopwatch the time I spent working on this test and I didn't have the 
chance to do it all in one sitting, but I estimate it took me about 5h/6h hours,
 including about 1 hour to write these notes (I hope they're helpful!).


## Running the app

In the project directory, you can run (you may use yarn instead of npm):

### `npm install`

Installs all dependencies / dev dependencies. This is required before you can
run the project

### `npm start`

Runs the app in the development mode.<br>
If a browser window doesn't automatically open, go to [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm build`

Builds the app into a deployable `/build` folder, containing the js bundle and 
static assets. 

For more commands and in depth information on the build setup, please visit 
[create-react-app's docs](https://github.com/facebookincubator/create-react-app/blob/master/packages/react-scripts/template/README.md).

## Dev notes

### File structure
The `public` folder contains assets outside of the module system (including the html 
base the react app is rendered in).

The `src` folder contains all the source code, including the entry point for the react app, `src/index.js`.

The code for each react component is contained within a folder inside /src. 
Component folders have capitalized names.

### Components

#### `App` 
  App root. It just renders the form.
#### `RegistrationForm`
  Contains the state management logic and renders the form
#### `BankAccount`
  Renders the inputs (iban+bank account) and delete button for a bank account.
#### `ValidatedInput`
  Renders a controlled input component with a label. Displays an error message
  if `props.error` is set.

### input fields configuration and validation
`src/config.js` exports a config object (`fieldsConfig`) keyed by field ID. 
Each field has a field name (`label` attribute) and an array of validation 
functions (`validators`). 
The priority of the error warnings is defined by the index of the validation 
function in the `validators` array.


### Styles
Style rules are stored in `.css` files.
They are imported by components (and later bundled by webpack), using es6 module
syntax, ex:`import './my-styles.css`.


## TODO
These are things that were left undone. 

I added some time estimates next to each item, although I'd like to note that 
in my experience time estimates in the early stages of a new project are often 
too optimistic, so please take these with a grain of salt.

#### Basic requirements
- styles for error warning in 'Bank accounts' section and buttons (20min)

- use proper format for output on form submit: at the moment, RegistrationForm's
 state object is being used (30min)

### Extras/improvements
- use [propType validation](https://facebook.github.io/react/docs/typechecking-with-proptypes.html) (30min) or even some kind of compile time type checking solution (flow/typescript).

- make layout adaptative for smaller width screens

- improve code comments (30min)

- move all components into a src/components folder so the file structure is 
cleaner (10min) 

- implement unit tests (3h) 

- after implementing unit tests, refactor the components so there is a reusable
Form component containing instead of having the RegistrationForm handling both user registraton 
and bank accounts forms. 
This would simplify the state update logic and reduce the complexity/code duplication
in the RegistrationForm component.
We'd have multiple Form instances, one for the user data and one for each bank account,
and the App component would manage the subscription process (4h) 

- UX bonus: use a relative unit like rem instead of px for font-size/layout 
dimensions, so the layout adjusts gracefully if the user uses a custom font-size
(30 min)


